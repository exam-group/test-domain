package com.exam.question.prepare

import com.exam.prepare.PrepareServiceProvider
import com.exam.prepare.api.SimpleQuestionService
import com.exam.prepare.model.SimpleQuestion
import com.exam.prepare.spi.Repository
import org.assertj.core.api.Assertions
import org.junit.jupiter.api.BeforeEach
import org.junit.jupiter.api.DisplayName
import org.junit.jupiter.api.Test
import org.junit.jupiter.api.TestInfo
import org.junit.jupiter.api.extension.ExtendWith
import org.junit.jupiter.params.ParameterizedTest
import org.junit.jupiter.params.provider.CsvSource
import org.junit.jupiter.params.provider.ValueSource
import org.mockito.Mock
import org.mockito.Mockito
import org.mockito.MockitoAnnotations
import org.mockito.Spy
import org.mockito.junit.jupiter.MockitoExtension

@ExtendWith(MockitoExtension::class)
class SimpleQuestionServiceTest {

    @Mock
    lateinit var repository: Repository<SimpleQuestion>

    @Spy
    lateinit var service: SimpleQuestionService

    @BeforeEach
    fun init() {
        MockitoAnnotations.initMocks(this)
        this.service = PrepareServiceProvider.factory.createSimpleQuestionService(repository)
    }

    @Test
    @DisplayName("a user should be able to read any existing SimpleQuestion")
    fun aUserShouldBeAbleToReadASimpleQuestion() {

        Mockito.`when`(repository.load(Mockito.anyString())).thenReturn(SimpleQuestion("0001", "test"))

        val question = this.service.readQuestion("0001")

        Assertions.assertThat(question).isNotNull()
        Assertions.assertThat(question!!.code).isEqualTo("0001")
        Assertions.assertThat(question!!.content).isEqualTo("test")
    }

    @Test
    @DisplayName("a user should not be able to read unexisting SimpleQuestion")
    fun aUserShouldNotBeAbleToReadAnUnexistingSimpleQuestion() {

        Mockito.`when`(repository.load(Mockito.anyString())).thenReturn(null)

        val question = this.service.readQuestion("0001")

        Assertions.assertThat(question).isNull()
    }

    @Test
    @DisplayName("a user should be able to create a SimpleQuestion")
    fun aUserShouldBeAbleToCreateASimpleQuestion() {

        Mockito.`when`(repository.generateCode()).thenReturn("0001")
        Mockito.`when`(repository.save(any())).thenAnswer { it.getArgument(0) }

        val question = this.service.createQuestion("test", "yes")
        Assertions.assertThat(question).isNotNull()
        Assertions.assertThat(question.code).isEqualTo("0001")
        Assertions.assertThat(question.expectations).containsExactly("yes")
        Mockito.verify(repository, Mockito.times(1)).save(any())
    }

    @Test
    @DisplayName("a user should be able to reset content of a SimpleQuestion")
    fun aUserShouldBeAbleToResetContentOfASimpleQuestion() {

        val question = SimpleQuestion("0001", "test", "yes")

        Mockito.`when`(repository.save(any())).thenAnswer { it.getArgument(0) }
        Mockito.`when`(repository.load(Mockito.anyString())).then { question }

        this.service.resetContent(question, "toto")

        val question2 = this.service.readQuestion("0001")
        Assertions.assertThat(question2).isNotNull()
        Assertions.assertThat(question2!!.content).isEqualTo("toto")
        Mockito.verify(repository, Mockito.times(1)).save(any())
    }

    @Test
    @DisplayName("a user should be able to delete an existing SimpleQuestion")
    fun aUserShouldBeAbleToDeleteAnExistingSimpleQuestion(testInfo: TestInfo) {

        val questions = mutableListOf(SimpleQuestion("0001", "test"))

        Mockito.`when`(repository.load(Mockito.anyString())).then { it1 ->
            questions.find {
                it.code == it1.arguments[0]
            }
        }

        Mockito.`when`(repository.delete(any())).then { it1 ->
            questions.removeIf {
                it.code == (it1.arguments[0] as SimpleQuestion).code
            }
        }

        var question = this.service.readQuestion("0001")
        this.service.deleteQuestions(question!!)
        question = this.service.readQuestion("0001")
        Assertions.assertThat(question).isNull()
        Mockito.verify(repository, Mockito.times(1)).delete(any())
    }

    @ParameterizedTest
    @CsvSource(value = arrayOf("A;B, B, A, 1", "A;B, C, A;B, 0", " , C, , 0"))
    @DisplayName("a user should be able to remove expectations from existing SimpleQuestion")
    fun aUserShouldBeAbleToRemoveExpectationsAnExistingSimpleQuestion(current: String?, element: String, expectedResult: String?, numSaveIsCalled: Int) {

        val expectations = if (current?.isNotEmpty() == true) {
            current.split(";").toMutableSet()
        } else null

        val expectedExpectations = if (expectedResult?.isNotEmpty() == true) {
            expectedResult.split(";").toMutableSet()
        } else null

        val questions = mutableListOf(SimpleQuestion("0001", "test", expectations))

        Mockito.`when`(repository.load(Mockito.anyString())).then { it1 ->
            questions.find {
                it.code == it1.arguments[0]
            }
        }

        Mockito.`when`(repository.exist(Mockito.anyString())).then { it1 ->
            questions.find {
                it.code == it1.arguments[0]
            } != null
        }


        var question = this.service.readQuestion("0001")
        this.service.removeExpectations(question!!, element)
        question = this.service.readQuestion("0001")
        Assertions.assertThat(question).isNotNull()
        if (expectedExpectations == null){
            Assertions.assertThat(question?.expectations).isNull()
        } else {
            Assertions.assertThat(question?.expectations).containsExactly(*expectedExpectations.toTypedArray())
        }
        Mockito.verify(repository, Mockito.times(numSaveIsCalled)).save(any())
    }

    @ParameterizedTest
    @CsvSource(value = arrayOf("A;B, B, A;B, 0", "A;B, C, A;B;C, 1", " , C, C, 1"))
    @DisplayName("a user should be able to add expectations to existing SimpleQuestion")
    fun aUserShouldBeAbleToAddExpectationsAnExistingSimpleQuestion(current: String?, element: String, expectedResult: String?, numSaveIsCalled: Int) {

        val expectations = if (current?.isNotEmpty() == true) {
            current.split(";").toMutableSet()
        } else null

        val expectedExpectations = if (expectedResult?.isNotEmpty() == true) {
            expectedResult.split(";").toMutableSet()
        } else null

        val questions = mutableListOf(SimpleQuestion("0001", "test", expectations))

        Mockito.`when`(repository.load(Mockito.anyString())).then { it1 ->
            questions.find {
                it.code == it1.arguments[0]
            }
        }

        Mockito.`when`(repository.exist(Mockito.anyString())).then { it1 ->
            questions.find {
                it.code == it1.arguments[0]
            } != null
        }

        var question = this.service.readQuestion("0001")
        this.service.addExpectations(question!!, element)
        question = this.service.readQuestion("0001")
        Assertions.assertThat(question).isNotNull()
        if (expectedExpectations == null) {
            Assertions.assertThat(question?.expectations).isNull()
        } else {
            Assertions.assertThat(question?.expectations).containsExactly(*expectedExpectations.toTypedArray())
        }
        Mockito.verify(repository, Mockito.times(numSaveIsCalled)).save(any())
    }

    @ParameterizedTest
    @CsvSource(value = arrayOf("A;B, B, B, 1", "A, C, C, 1", " , C, C, 1"))
    @DisplayName("a user should be able to reset expectations to existing SimpleQuestion")
    fun aUserShouldBeAbleToResetExpectationsAnExistingSimpleQuestion(current: String?, element: String, expectedResult: String?, numSaveIsCalled: Int) {

        val expectations = if (current?.isNotEmpty() == true) {
            current.split(";").toMutableSet()
        } else null

        val expectedExpectations = if (expectedResult?.isNotEmpty() == true) {
            expectedResult.split(";").toMutableSet()
        } else null

        val questions = mutableListOf(SimpleQuestion("0001", "test", expectations))

        Mockito.`when`(repository.load(Mockito.anyString())).then { it1 ->
            questions.find {
                it.code == it1.arguments[0]
            }
        }

        Mockito.`when`(repository.exist(Mockito.anyString())).then { it1 ->
            questions.find {
                it.code == it1.arguments[0]
            } != null
        }

        var question = this.service.readQuestion("0001")
        this.service.resetExpectations(question!!, element)
        question = this.service.readQuestion("0001")
        Assertions.assertThat(question).isNotNull()
        if (expectedExpectations == null) {
            Assertions.assertThat(question?.expectations).isNull()
        } else {
            Assertions.assertThat(question?.expectations).containsExactly(*expectedExpectations.toTypedArray())
        }
        Mockito.verify(repository, Mockito.times(numSaveIsCalled)).save(any())
    }
}