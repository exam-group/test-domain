package com.exam.question.prepare

import com.exam.prepare.PrepareServiceProvider
import com.exam.prepare.api.MultipleChoiceQuestionService
import com.exam.prepare.model.Expectation
import com.exam.prepare.model.MultipleChoiceQuestion
import com.exam.prepare.model.Proposal
import com.exam.prepare.model.SimpleQuestion
import com.exam.prepare.spi.Repository
import org.assertj.core.api.Assertions
import org.junit.jupiter.api.BeforeEach
import org.junit.jupiter.api.DisplayName
import org.junit.jupiter.api.Test
import org.junit.jupiter.api.TestInfo
import org.junit.jupiter.api.extension.ExtendWith
import org.mockito.Mock
import org.mockito.Mockito
import org.mockito.MockitoAnnotations
import org.mockito.Spy
import org.mockito.junit.jupiter.MockitoExtension

@ExtendWith(MockitoExtension::class)
class MultipleChoiceQuestionServiceTest {

    @Mock
    lateinit var repository: Repository<MultipleChoiceQuestion>

    @Spy
    lateinit var service: MultipleChoiceQuestionService

    @BeforeEach
    fun init() {
        MockitoAnnotations.initMocks(this)
        this.service = PrepareServiceProvider.factory.createMultipleChoiceQuestionService(repository)
    }

    @Test
    @DisplayName("a user should be able to read any existing MultipleChoiceQuestion")
    fun aUserShouldBeAbleToReadAMultipleChoiceQuestion() {

        Mockito.`when`(repository.load(Mockito.anyString())).thenReturn(MultipleChoiceQuestion("0001", "test"))

        val question = this.service.readQuestion("0001")

        Assertions.assertThat(question).isNotNull()
        Assertions.assertThat(question!!.code).isEqualTo("0001")
        Assertions.assertThat(question!!.content).isEqualTo("test")
    }

    @Test
    @DisplayName("a user should not be able to read unexisting MultipleChoiceQuestion")
    fun aUserShouldNotBeAbleToReadAnUnexistingMultipleChoiceQuestion() {

        Mockito.`when`(repository.load(Mockito.anyString())).thenReturn(null)

        val question = this.service.readQuestion("0001")

        Assertions.assertThat(question).isNull()
    }

    @Test
    @DisplayName("a user should be able to create a MultipleChoiceQuestion")
    fun aUserShouldBeAbleToCreateAMultipleChoiceQuestion() {

        Mockito.`when`(repository.generateCode()).thenReturn("0001")
        Mockito.`when`(repository.save(any())).thenAnswer { it.getArgument(0) }

        val question = this.service.createQuestion("test", Expectation(Proposal("yes"), true))
        Assertions.assertThat(question).isNotNull()
        Assertions.assertThat(question.code).isEqualTo("0001")
        Assertions.assertThat(question.expectations).containsExactly(Expectation(Proposal("yes"), true))
        Mockito.verify(repository, Mockito.times(1)).save(any())
    }

    @Test
    @DisplayName("a user should be able to reset content of a MultipleChoiceQuestion")
    fun aUserShouldBeAbleToResetContentOfAMultipleChoiceQuestion() {

        val question = MultipleChoiceQuestion("0001", "test")

        Mockito.`when`(repository.save(any())).thenAnswer { it.getArgument(0) }
        Mockito.`when`(repository.load(Mockito.anyString())).then { question }

        this.service.resetContent(question, "toto")

        val question2 = this.service.readQuestion("0001")
        Assertions.assertThat(question2).isNotNull()
        Assertions.assertThat(question2!!.content).isEqualTo("toto")
        Mockito.verify(repository, Mockito.times(1)).save(any())
    }

    @Test
    @DisplayName("a user should be able to delete an existing MultipleChoiceQuestion")
    fun aUserShouldBeAbleToDeleteAnExistingMultipleChoiceQuestion(testInfo: TestInfo) {

        val questions = mutableListOf(MultipleChoiceQuestion("0001", "test"))

        Mockito.`when`(repository.load(Mockito.anyString())).then { it1 ->
            questions.find {
                it.code == it1.arguments[0]
            }
        }

        Mockito.`when`(repository.delete(any())).then { it1 ->
            questions.removeIf {
                it.code == (it1.arguments[0] as MultipleChoiceQuestion).code
            }
        }

        var question = this.service.readQuestion("0001")
        this.service.deleteQuestions(question!!)
        question = this.service.readQuestion("0001")
        Assertions.assertThat(question).isNull()
        Mockito.verify(repository, Mockito.times(1)).delete(any())
    }

    @Test
    @DisplayName("a user should be able to remove expectations from existing MultipleChoiceQuestion")
    fun aUserShouldBeAbleToRemoveExpectationsAnExistingMultipleChoiceQuestion(testInfo: TestInfo) {

        val questions = mutableListOf(MultipleChoiceQuestion("0001", "test", mutableSetOf(Expectation(Proposal("yes"), true), Expectation(Proposal("no"), false), Expectation(Proposal("maybe"), false))))

        Mockito.`when`(repository.load(Mockito.anyString())).then { it1 ->
            questions.find {
                it.code == it1.arguments[0]
            }
        }

        Mockito.`when`(repository.exist(Mockito.anyString())).then { it1 ->
            questions.find {
                it.code == it1.arguments[0]
            } != null
        }

        var question = this.service.readQuestion("0001")
        this.service.removeExpectations(question!!, Expectation(Proposal("maybe"), false))
        question = this.service.readQuestion("0001")
        Assertions.assertThat(question).isNotNull()
        Assertions.assertThat(question?.expectations!!).containsExactly(Expectation(Proposal("yes"), true), Expectation(Proposal("no"), false))
        Mockito.verify(repository, Mockito.times(1)).save(any())
    }

    @Test
    @DisplayName("a user should be able to add expectations to existing MultipleChoiceQuestion")
    fun aUserShouldBeAbleToAddExpectationsAnExistingMultipleChoiceQuestion(testInfo: TestInfo) {

        val questions = mutableListOf(MultipleChoiceQuestion("0001", "test", mutableSetOf(Expectation(Proposal("yes"), true))))

        Mockito.`when`(repository.load(Mockito.anyString())).then { it1 ->
            questions.find {
                it.code == it1.arguments[0]
            }
        }

        Mockito.`when`(repository.exist(Mockito.anyString())).then { it1 ->
            questions.find {
                it.code == it1.arguments[0]
            } != null
        }

        var question = this.service.readQuestion("0001")
        this.service.addExpectations(question!!, Expectation(Proposal("no"), false))
        question = this.service.readQuestion("0001")
        Assertions.assertThat(question).isNotNull()
        Assertions.assertThat(question?.expectations!!).containsExactly(Expectation(Proposal("yes"), true), Expectation(Proposal("no"), false))
        Mockito.verify(repository, Mockito.times(1)).save(any())
    }

    @Test
    @DisplayName("a user should be able to reset expectations to existing MultipleChoiceQuestion")
    fun aUserShouldBeAbleToResetExpectationsAnExistingMultipleChoiceQuestion(testInfo: TestInfo) {

        val questions = mutableListOf(MultipleChoiceQuestion("0001", "test", mutableSetOf(Expectation(Proposal("yes"), true), Expectation(Proposal("no"), false))))

        Mockito.`when`(repository.load(Mockito.anyString())).then { it1 ->
            questions.find {
                it.code ==it1.arguments[0]
            }
        }

        Mockito.`when`(repository.exist(Mockito.anyString())).then { it1 ->
            questions.find {
                it.code == it1.arguments[0]
            } != null
        }

        var question = this.service.readQuestion("0001")
        this.service.resetExpectations(question!!, Expectation(Proposal("TRUE"), true), Expectation(Proposal("FALSE"), false))
        question = this.service.readQuestion("0001")
        Assertions.assertThat(question).isNotNull()
        Assertions.assertThat(question?.expectations!!).containsExactly(Expectation(Proposal("TRUE"), true), Expectation(Proposal("FALSE"), false))
        Mockito.verify(repository, Mockito.times(1)).save(any())
    }


}