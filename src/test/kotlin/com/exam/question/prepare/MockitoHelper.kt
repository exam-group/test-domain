package com.exam.question.prepare

import org.mockito.Mockito

// Mockito.any can be null which is forbidden by Kotlin --> use this method instead of Mockito.any<T>()
fun <T> any(): T {
    Mockito.any<T>()
    return null as T
}