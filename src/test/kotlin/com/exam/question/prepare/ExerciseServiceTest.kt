package com.exam.question.prepare

import com.exam.prepare.PrepareServiceProvider
import com.exam.prepare.api.ExerciseService
import com.exam.prepare.model.*
import com.exam.prepare.spi.Repository
import org.assertj.core.api.Assertions
import org.junit.jupiter.api.BeforeEach
import org.junit.jupiter.api.DisplayName
import org.junit.jupiter.api.Test
import org.junit.jupiter.api.TestInfo
import org.junit.jupiter.api.extension.ExtendWith
import org.mockito.Mock
import org.mockito.Mockito
import org.mockito.MockitoAnnotations
import org.mockito.Spy
import org.mockito.junit.jupiter.MockitoExtension
import java.time.temporal.ChronoUnit

@ExtendWith(MockitoExtension::class)
class ExerciseServiceTest {

    @Mock
    lateinit var repository: Repository<Exercise>

    @Spy
    lateinit var service: ExerciseService

    @BeforeEach
    fun init() {
        MockitoAnnotations.initMocks(this)
        this.service = PrepareServiceProvider.factory.createExerciseService(repository)
    }

    @Test
    @DisplayName("a user should be able to read any existing Exercise")
    fun aUserShouldBeAbleToReadAnExercise() {

        Mockito.`when`(repository.load(Mockito.anyString())).thenReturn(Exercise("0001", "test"))

        val question = this.service.readExercise("0001")

        Assertions.assertThat(question).isNotNull()
        Assertions.assertThat(question!!.code).isEqualTo("0001")
    }

    @Test
    @DisplayName("a user should not be able to read unexisting Exercise")
    fun aUserShouldNotBeAbleToReadAnUnexistingExercise() {

        Mockito.`when`(repository.load(Mockito.anyString())).thenReturn(null)

        val exercise = this.service.readExercise("0001")

        Assertions.assertThat(exercise).isNull()
    }

    @Test
    @DisplayName("a user should be able to create a Exercise")
    fun aUserShouldBeAbleToCreateAnExercise() {

        Mockito.`when`(repository.generateCode()).thenReturn("0001")
        Mockito.`when`(repository.save(any())).thenAnswer { it.getArgument(0) }

        val question = SimpleQuestion("1", "test")
        val exercise = this.service.createExercise("test", question)
        Assertions.assertThat(exercise).isNotNull()
        Assertions.assertThat(exercise.code).isEqualTo("0001")
        Assertions.assertThat(exercise.questions).containsExactly(question)
        Mockito.verify(repository, Mockito.times(1)).save(any())
    }

    @Test
    @DisplayName("a user should be able to delete an existing Exercise")
    fun aUserShouldBeAbleToDeleteAnExistingExercise(testInfo: TestInfo) {

        val exercises = mutableListOf(Exercise("0001", "test"))

        Mockito.`when`(repository.load(Mockito.anyString())).then { it1 ->
            exercises.find {
                it.code == it1.arguments[0]
            }
        }

        Mockito.`when`(repository.delete(any())).then { it1 ->
            exercises.removeIf {
                it.code == (it1.arguments[0] as Exercise).code
            }
        }

        var exercise = this.service.readExercise("0001")
        this.service.deleteExercise(exercise!!)
        exercise = this.service.readExercise("0001")
        Assertions.assertThat(exercise).isNull()
        Mockito.verify(repository, Mockito.times(1)).delete(any())
    }

    @Test
    @DisplayName("a user should be able to remove questions from existing Exercise")
    fun aUserShouldBeAbleToRemoveQuestionAnExistingExercise(testInfo: TestInfo) {

        val question = SimpleQuestion("0001", "test")
        val exercises = mutableListOf(Exercise("0001", "test", question))

        Mockito.`when`(repository.load(Mockito.anyString())).then { it1 ->
            exercises.find {
                it.code == it1.arguments[0]
            }
        }

        Mockito.`when`(repository.exist(Mockito.anyString())).then { it1 ->
            exercises.find {
                it.code == it1.arguments[0]
            } != null
        }

        var exercise = this.service.readExercise("0001")
        this.service.removeQuestions(exercise!!, question)
        exercise = this.service.readExercise("0001")
        Assertions.assertThat(exercise!!.questions).isEmpty()
        Mockito.verify(repository, Mockito.times(1)).save(any())
    }

    @Test
    @DisplayName("a user should be able to add question to existing Exercise")
    fun aUserShouldBeAbleToAddQuestionAnExistingExercise(testInfo: TestInfo) {

        val question = SimpleQuestion("0001", "test")
        val exercises = mutableListOf(Exercise("0001", "test", question))

        Mockito.`when`(repository.load(Mockito.anyString())).then { it1 ->
            exercises.find {
                it.code == it1.arguments[0]
            }
        }

        Mockito.`when`(repository.exist(Mockito.anyString())).then { it1 ->
            exercises.find {
                it.code == it1.arguments[0]
            } != null
        }

        val question2 = SimpleQuestion("0002", "test2")
        var exercise = this.service.readExercise("0001")
        this.service.addQuestions(exercise!!, question2)
        exercise = this.service.readExercise("0001")
        Assertions.assertThat(exercise).isNotNull()
        Assertions.assertThat(exercise?.questions).containsExactly(question, question2)
        Mockito.verify(repository, Mockito.times(1)).save(any())
    }


    @Test
    @DisplayName("a user should be able to add question at first position to existing Exercise")
    fun aUserShouldBeAbleToAddQuestionAtFirstPositionAnExistingExercise(testInfo: TestInfo) {

        val question = SimpleQuestion("0001", "test")
        val exercises = mutableListOf(Exercise("0001", "test", question))

        Mockito.`when`(repository.load(Mockito.anyString())).then { it1 ->
            exercises.find {
                it.code == it1.arguments[0]
            }
        }

        Mockito.`when`(repository.exist(Mockito.anyString())).then { it1 ->
            exercises.find {
                it.code == it1.arguments[0]
            } != null
        }

        val question2 = SimpleQuestion("0002", "test2")
        var exercise = this.service.readExercise("0001")
        this.service.addQuestions(exercise!!, question2, index = 0)
        exercise = this.service.readExercise("0001")
        Assertions.assertThat(exercise).isNotNull()
        Assertions.assertThat(exercise?.questions).containsExactly(question2, question)
        Mockito.verify(repository, Mockito.times(1)).save(any())
    }

    @Test
    @DisplayName("a user should be able to reset question to existing Exercise")
    fun aUserShouldBeAbleToResetQuestionAnExistingExercise(testInfo: TestInfo) {

        val question = SimpleQuestion("0001", "test")
        val exercises = mutableListOf(Exercise("0001", "test", question))

        Mockito.`when`(repository.load(Mockito.anyString())).then { it1 ->
            exercises.find {
                it.code == it1.arguments[0]
            }
        }

        Mockito.`when`(repository.exist(Mockito.anyString())).then { it1 ->
            exercises.find {
                it.code == it1.arguments[0]
            } != null
        }

        val question2 = SimpleQuestion("0002", "test2")
        var exercise = this.service.readExercise("0001")
        this.service.resetQuestions(exercise!!, question2)
        exercise = this.service.readExercise("0001")
        Assertions.assertThat(exercise).isNotNull()
        Assertions.assertThat(exercise?.questions).containsExactly(question2)
        Mockito.verify(repository, Mockito.times(1)).save(any())
    }

    @Test
    @DisplayName("a user should be able to move question inside existing Exercise")
    fun aUserShouldBeAbleToMoveQuestionInsideExercise(testInfo: TestInfo) {

        val questions: Map<Int, Question> = mapOf(
                1 to SimpleQuestion("0001", "test"),
                2 to SimpleQuestion("0002", "test"),
                3 to SimpleQuestion("0003", "test"),
                4 to SimpleQuestion("0004", "test"))

        val exercises = mutableListOf(Exercise("0001", "test", questions.values.toMutableList()))

        Mockito.`when`(repository.load(Mockito.anyString())).then { it1 ->
            exercises.find {
                it.code == it1.arguments[0]
            }
        }

        Mockito.`when`(repository.exist(Mockito.anyString())).then { it1 ->
            exercises.find {
                it.code == it1.arguments[0]
            } != null
        }

        var exercise = this.service.readExercise("0001")
        this.service.moveQuestions(exercise!!, questions[2]!!, questions[3]!!, index = 0)
        exercise = this.service.readExercise("0001")
        Assertions.assertThat(exercise).isNotNull()
        Assertions.assertThat(exercise?.questions).containsExactly(questions[2]!!, questions[3]!!, questions[1]!!, questions[4]!!)
        Mockito.verify(repository, Mockito.times(1)).save(any())

    }

    @Test
    @DisplayName("a user should be able to specify the difficulty level of any Exercise")
    fun aUserShouldBeAbleToSpecifyTheDifficultyLevelOfAnyExercise(testInfo: TestInfo) {

        val exercises = mutableListOf(Exercise("0001", "test"))

        Mockito.`when`(repository.load(Mockito.anyString())).then { it1 ->
            exercises.find {
                it.code == it1.arguments[0]
            }
        }

        Mockito.`when`(repository.exist(Mockito.anyString())).then { it1 ->
            exercises.find {
                it.code == it1.arguments[0]
            } != null
        }

        var exercise = this.service.readExercise("0001")
        this.service.specifyDifficultyLevel(exercise!!, Level.HARD)
        exercise = this.service.readExercise("0001")
        Assertions.assertThat(exercise).isNotNull()
        Assertions.assertThat(exercise?.difficulty).isEqualTo(Level.HARD)
        Mockito.verify(repository, Mockito.times(1)).save(any())
    }

    @Test
    @DisplayName("a user should be able to specify the duration of any Exercise")
    fun aUserShouldBeAbleToSpecifyTheDurationOfAnyExercise(testInfo: TestInfo) {

        val exercises = mutableListOf(Exercise("0001", "test"))

        Mockito.`when`(repository.load(Mockito.anyString())).then { it1 ->
            exercises.find {
                it.code == it1.arguments[0]
            }
        }

        Mockito.`when`(repository.exist(Mockito.anyString())).then { it1 ->
            exercises.find {
                it.code == it1.arguments[0]
            } != null
        }

        var exercise = this.service.readExercise("0001")
        this.service.specifyDuration(exercise!!, Chrono(10, ChronoUnit.MINUTES))
        exercise = this.service.readExercise("0001")
        Assertions.assertThat(exercise).isNotNull()
        Assertions.assertThat(exercise?.duration).isEqualTo(Chrono(10, ChronoUnit.MINUTES))
        Mockito.verify(repository, Mockito.times(1)).save(any())
    }

    @Test
    @DisplayName("a user should be able to specify the total point of any Exercise")
    fun aUserShouldBeAbleToSpecifyTheTotalPointOfAnyExercise(testInfo: TestInfo) {

        val exercises = mutableListOf(Exercise("0001", "test"))

        Mockito.`when`(repository.load(Mockito.anyString())).then { it1 ->
            exercises.find {
                it.code == it1.arguments[0]
            }
        }

        Mockito.`when`(repository.exist(Mockito.anyString())).then { it1 ->
            exercises.find {
                it.code == it1.arguments[0]
            } != null
        }

        var exercise = this.service.readExercise("0001")
        this.service.specifyTotalPoint(exercise!!, 10)
        exercise = this.service.readExercise("0001")
        Assertions.assertThat(exercise).isNotNull()
        Assertions.assertThat(exercise?.totalPoint).isEqualTo(10)
        Mockito.verify(repository, Mockito.times(1)).save(any())
    }

}