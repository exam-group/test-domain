package com.exam.prepare.business

import com.exam.prepare.api.SimpleQuestionService
import com.exam.prepare.model.SimpleQuestion
import com.exam.prepare.spi.Repository

internal class SimpleQuestionServiceImpl(repository: Repository<SimpleQuestion>) : AdtQuestionService<SimpleQuestion>(repository), SimpleQuestionService {

    override fun readQuestion(code: String): SimpleQuestion? {
        return repository.load(code)
    }

    override fun createQuestion(content: String, vararg expectations: String): SimpleQuestion {
        val question = SimpleQuestion(repository.generateCode(), content, *expectations);
        return repository.save(question)
    }

    override fun removeExpectations(question: SimpleQuestion, vararg expectations: String) {

        // Remove Expectation when question exists and at least one of given expectations belongs to question
        question.inCaseWhen { this.exist() && this.expectations?.any { expectations.contains(it) } ?: false }
                .then {
                    this.expectations?.removeIf { expectations.contains(it) }
                    repository.save(this)
                }
    }

    override fun addExpectations(question: SimpleQuestion, vararg expectations: String) {

        // Add expectation when question exists and at least one of given expectations does not belong to question
        question.inCaseWhen { this.exist() && this.expectations?.any{ expectations.contains(it)}?.not() ?: true}
                .then {
                    if (this.expectations == null) {
                        this.expectations = mutableSetOf(*expectations)
                    } else {
                        this.expectations?.addAll(expectations)
                    }
                }
                .then { repository.save(this) }
    }

    override fun resetExpectations(question: SimpleQuestion, vararg expectations: String) {


        question.inCaseWhen { this.exist() }
                .then {
                    if (this.expectations == null) {
                        this.expectations = mutableSetOf(*expectations)
                    } else {
                        this.expectations?.clear()
                        this.expectations?.addAll(expectations)
                    }
                }
                .then { repository.save(this) }
    }
}