package com.exam.prepare.business

import com.exam.prepare.api.QuestionService
import com.exam.prepare.model.Question
import com.exam.prepare.spi.Repository

internal abstract class AdtQuestionService<T : Question>(val repository: Repository<T>) : QuestionService<T> {

    override fun resetContent(question: T, content: String) {
        question.content = content
        repository.save(question)
    }

    override fun deleteQuestions(vararg questions: T) {
        questions.forEach {
            repository.delete(it)
        }
    }

    protected fun T.exist(): Boolean {
        return repository.exist(this.code)
    }
}