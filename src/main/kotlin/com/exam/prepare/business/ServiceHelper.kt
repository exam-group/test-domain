package com.exam.prepare.business

internal inline fun <reified T> T.inCaseWhen(predicate: T.() -> Boolean): Accepted<T>? {

    return if (predicate(this)) {
        Accepted(this)
    } else null
}

class Accepted<T>(val element: T)

internal inline fun <reified T> Accepted<T>?.then(action: T.() -> Unit): Accepted<T>? {

    this?.let {
        action(this.element)
        return this
    }
    return null
}