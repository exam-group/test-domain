package com.exam.prepare.business

import com.exam.prepare.api.MultipleChoiceQuestionService
import com.exam.prepare.model.Expectation
import com.exam.prepare.model.MultipleChoiceQuestion
import com.exam.prepare.spi.Repository

internal class MultipleChoiceQuestionServiceImpl(repository: Repository<MultipleChoiceQuestion>) : AdtQuestionService<MultipleChoiceQuestion>(repository), MultipleChoiceQuestionService {

    override fun readQuestion(code: String): MultipleChoiceQuestion? {
        return repository.load(code)
    }

    override fun createQuestion(content: String, vararg expectations: Expectation): MultipleChoiceQuestion {

        val question = MultipleChoiceQuestion(repository.generateCode(), content, *expectations)
        return repository.save(question)
    }

    override fun removeExpectations(question: MultipleChoiceQuestion, vararg expectations: Expectation) {

        // Remove Expectation when question exists and at least one of given expectations belongs to question
        question.inCaseWhen { this.exist() && this.expectations.stream().anyMatch { expectations.contains(it) } }
                .then {
                    this.expectations.removeIf { it1 -> expectations.map { it.proposal.content }.contains(it1.proposal.content) }
                    repository.save(this)
                }
    }

    override fun addExpectations(question: MultipleChoiceQuestion, vararg expectations: Expectation) {

        question.inCaseWhen { this.exist() }
                .then {
                    this.expectations.addAll(expectations)
                    repository.save(this)
                }
    }

    override fun resetExpectations(question: MultipleChoiceQuestion, vararg expectations: Expectation) {

        question.inCaseWhen { this.exist() }
                .then {
                    with(this.expectations) {
                        this.clear()
                        this.addAll(expectations)
                    }
                }
                .then { repository.save(this) }
    }
}