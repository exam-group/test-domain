package com.exam.prepare.business

import com.exam.prepare.api.ExerciseService
import com.exam.prepare.model.Chrono
import com.exam.prepare.model.Exercise
import com.exam.prepare.model.Level
import com.exam.prepare.model.Question
import com.exam.prepare.spi.Repository

internal class ExerciseServiceImpl(val repository: Repository<Exercise>) : ExerciseService {

    override fun readExercise(code: String): Exercise? {
        return repository.load(code)
    }

    override fun createExercise(name: String, vararg questions: Question): Exercise {
        val exercise = Exercise(repository.generateCode(), name, *questions);
        return repository.save(exercise)
    }

    override fun deleteExercise(vararg exercises: Exercise) {
        exercises.forEach {
            repository.delete(it)
        }
    }

    override fun addQuestions(exercise: Exercise, vararg questions: Question, index: Int) {

        exercise.inCaseWhen { this.exist() }
                .then {
                    arrayListOf(*questions).stream().filter {
                        !exercise.questions.contains(it)
                    }.forEach {
                        exercise.questions.add(index, it)
                    }
                }
                .then { repository.save(this) }

    }

    override fun removeQuestions(exercise: Exercise, vararg questions: Question) {

        // Remove questions when exercise exists and at least one of given questions belongs to exercise
        exercise.inCaseWhen { this.exist() && this.questions.any { questions.contains(it) } }
                .then {
                    this.questions.removeIf { questions.contains(it) }
                    repository.save(this)
                }
    }

    override fun resetQuestions(exercise: Exercise, vararg questions: Question) {

        exercise.inCaseWhen { this.exist() }
                .then {
                    with(this.questions) {
                        this.clear()
                        this.addAll(questions)
                    }
                }
                .then { repository.save(this) }
    }

    override fun moveQuestions(exercise: Exercise, vararg questions: Question, index: Int) {

        exercise.inCaseWhen { this.exist() && questions.all { this.questions.contains(it) } }
                .then {
                    this.questions.removeIf { questions.contains(it) }
                    this.questions.addAll(index, arrayListOf(*questions))
                }
                .then { repository.save(this) }
    }

    override fun specifyDifficultyLevel(exercise: Exercise, difficulty: Level) {

        exercise.inCaseWhen { this.exist() && this.difficulty != difficulty }
                .then {
                    this.difficulty = difficulty
                    repository.save(this)
                }
    }

    override fun specifyDuration(exercise: Exercise, duration: Chrono) {

        exercise.inCaseWhen { this.exist() && this.duration != duration }
                .then {
                    this.duration = duration
                    repository.save(this)
                }
    }

    override fun specifyTotalPoint(exercise: Exercise, totalPoint: Int) {

        exercise.inCaseWhen { this.exist() && this.totalPoint != totalPoint }
                .then {
                    this.totalPoint = totalPoint
                    repository.save(this)
                }
    }

    private fun Exercise.exist(): Boolean {
        return repository.exist(this.code)
    }
}