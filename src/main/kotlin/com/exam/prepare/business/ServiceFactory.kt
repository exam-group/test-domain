package com.exam.prepare.business

import com.exam.prepare.api.ExerciseService
import com.exam.prepare.api.MultipleChoiceQuestionService
import com.exam.prepare.api.SimpleQuestionService
import com.exam.prepare.model.Exercise
import com.exam.prepare.model.MultipleChoiceQuestion
import com.exam.prepare.model.SimpleQuestion
import com.exam.prepare.spi.Repository

object ServiceFactory {

    fun createSimpleQuestionService(repository: Repository<SimpleQuestion>): SimpleQuestionService {
        return SimpleQuestionServiceImpl(repository)
    }

    fun createMultipleChoiceQuestionService(repository: Repository<MultipleChoiceQuestion>): MultipleChoiceQuestionService {
        return MultipleChoiceQuestionServiceImpl(repository)
    }

        fun createExerciseService(repository: Repository<Exercise>): ExerciseService {
        return ExerciseServiceImpl(repository)
    }
}