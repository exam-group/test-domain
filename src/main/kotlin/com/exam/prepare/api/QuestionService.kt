package com.exam.prepare.api

import com.exam.prepare.model.Question

interface QuestionService<T: Question> {

    fun readQuestion(code: String): T?

    fun resetContent(question: T, content: String)

    fun deleteQuestions(vararg questions: T)
}

