package com.exam.prepare.api

import com.exam.prepare.model.Expectation
import com.exam.prepare.model.MultipleChoiceQuestion

interface MultipleChoiceQuestionService : QuestionService<MultipleChoiceQuestion> {

    fun createQuestion(content: String, vararg expectations: Expectation): MultipleChoiceQuestion

    fun removeExpectations(question: MultipleChoiceQuestion, vararg expectations: Expectation)

    fun addExpectations(question: MultipleChoiceQuestion, vararg expectations: Expectation)

    fun resetExpectations(question: MultipleChoiceQuestion, vararg expectations: Expectation)
}