package com.exam.prepare.api

import com.exam.prepare.model.SimpleQuestion

interface SimpleQuestionService : QuestionService<SimpleQuestion> {

    fun createQuestion(content: String, vararg expectations: String): SimpleQuestion

    fun removeExpectations(question: SimpleQuestion, vararg expectations: String)

    fun addExpectations(question: SimpleQuestion, vararg expectations: String)

    fun resetExpectations(question: SimpleQuestion, vararg expectations: String)
}