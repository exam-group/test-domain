package com.exam.prepare.api

import com.exam.prepare.model.Chrono
import com.exam.prepare.model.Exercise
import com.exam.prepare.model.Level
import com.exam.prepare.model.Question

interface ExerciseService {

    fun readExercise(code: String): Exercise?

    fun createExercise(name: String, vararg questions: Question): Exercise

    fun deleteExercise(vararg exercises: Exercise)

    fun addQuestions(exercise: Exercise, vararg questions: Question, index: Int = exercise.questions.size )

    fun removeQuestions(exercise: Exercise, vararg questions: Question)

    fun resetQuestions(exercise: Exercise, vararg questions: Question)

    fun moveQuestions(exercise: Exercise, vararg questions: Question, index: Int)

    fun specifyDifficultyLevel(exercise: Exercise, difficulty: Level)

    fun specifyDuration(exercise: Exercise, duration: Chrono)

    fun specifyTotalPoint(exercise: Exercise, totalPoint: Int)
}