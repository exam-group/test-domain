package com.exam.prepare

import com.exam.prepare.business.ServiceFactory

object PrepareServiceProvider {

    val factory = ServiceFactory
}
