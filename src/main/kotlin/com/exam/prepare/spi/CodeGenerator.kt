package com.exam.prepare.spi

interface CodeGenerator {

    fun generateCode(): String
}