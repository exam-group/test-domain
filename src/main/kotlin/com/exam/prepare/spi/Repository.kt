package com.exam.prepare.spi

interface Repository<T> : CodeGenerator {

    fun exist(code: String): Boolean
    
    fun load(code: String): T?

    fun save(element: T): T

    fun delete(element: T)
}