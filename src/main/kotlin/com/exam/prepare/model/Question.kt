package com.exam.prepare.model

interface Question {

    val code: String

    var content: String
}