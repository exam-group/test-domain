package com.exam.prepare.model

data class SimpleQuestion(override val code: String, override var content: String, var expectations: MutableSet<String>? = null) : Question {

    constructor(code: String, content: String, vararg expectations: String) : this(code, content, mutableSetOf(*expectations))
}
