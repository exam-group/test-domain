package com.exam.prepare.model

import java.time.temporal.ChronoUnit

data class Exercise(val code: String, val title: String, val questions: MutableList<Question> = mutableListOf()) {

    constructor(code: String, title: String, vararg questions: Question) : this(code, title, mutableListOf(*questions))

    var difficulty: Level = Level.TRIVIAL

    var duration: Chrono = Chrono(5, ChronoUnit.MINUTES)

    var totalPoint: Int = 0
}


