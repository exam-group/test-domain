package com.exam.prepare.model

import java.time.temporal.ChronoUnit

data class Chrono(val time: Int, val unit: ChronoUnit)