package com.exam.prepare.model

data class Expectation(val proposal: Proposal, val isExpected: Boolean)
