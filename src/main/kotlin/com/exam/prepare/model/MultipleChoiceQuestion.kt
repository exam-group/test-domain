package com.exam.prepare.model

data class MultipleChoiceQuestion(override val code: String,
                                  override var content: String,
                                  val expectations: MutableSet<Expectation> = mutableSetOf()) : Question {

    constructor(code: String, content: String, vararg expectations: Expectation) : this(code, content, mutableSetOf(*expectations))
}
