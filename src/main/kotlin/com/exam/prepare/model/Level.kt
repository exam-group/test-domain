package com.exam.prepare.model

enum class Level(val code: Int) {
    IMPOSSIBLE(0), HARD(1), MEDIUM(2), EASY(3), TRIVIAL(4)
}