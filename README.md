# test-domain

To host maven repo in gitlab : https://cemerick.com/2010/08/24/hosting-maven-repos-on-github/

Deploy Snapshot:
   * deploy -DaltDeploymentRepository=gitlab::default::file:"D:/Olivier/Projets Java/Repositories-GitLab-Main/ote-repository/snapshot"
   * git push D:/Olivier/Projets Java/Repositories-GitLab-Main/ote-repository/snapshot

Release:
    * clean release:clean release:prepare -Dtag=REL_${releaseVersion} -DreleaseVersion=1.0.1 -DdevelopmentVersion=1.0.2-SNAPSHOT
    * deploy -DaltDeploymentRepository=gitlab::default::file:"D:/Olivier/Projets Java/Repositories-GitLab-Main/ote-repository/release"

    * release:perform -Dusername=oterrien@neuf.fr -Dpassword=**********

This last point does not work but even without everything seems to be ok